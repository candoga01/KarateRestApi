var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "272",
        "ok": "143",
        "ko": "129"
    },
    "minResponseTime": {
        "total": "68",
        "ok": "434",
        "ko": "68"
    },
    "maxResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "126"
    },
    "meanResponseTime": {
        "total": "367",
        "ok": "624",
        "ko": "83"
    },
    "standardDeviation": {
        "total": "295",
        "ok": "164",
        "ko": "9"
    },
    "percentiles1": {
        "total": "460",
        "ok": "537",
        "ko": "82"
    },
    "percentiles2": {
        "total": "548",
        "ok": "751",
        "ko": "87"
    },
    "percentiles3": {
        "total": "782",
        "ok": "846",
        "ko": "97"
    },
    "percentiles4": {
        "total": "952",
        "ok": "1140",
        "ko": "115"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 132,
        "percentage": 49
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 9,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 2,
        "percentage": 1
    },
    "group4": {
        "name": "failed",
        "count": 129,
        "percentage": 47
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "5.037",
        "ok": "2.648",
        "ko": "2.389"
    }
},
contents: {
"req_post--api-users-7a661": {
        type: "REQUEST",
        name: "POST /api/users/login",
path: "POST /api/users/login",
pathFormatted: "req_post--api-users-7a661",
stats: {
    "name": "POST /api/users/login",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 1,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.019",
        "ok": "0.019",
        "ko": "-"
    }
}
    },"req_post--api-artic-043e6": {
        type: "REQUEST",
        name: "POST /api/articles",
path: "POST /api/articles",
pathFormatted: "req_post--api-artic-043e6",
stats: {
    "name": "POST /api/articles",
    "numberOfRequests": {
        "total": "156",
        "ok": "115",
        "ko": "41"
    },
    "minResponseTime": {
        "total": "68",
        "ok": "453",
        "ko": "68"
    },
    "maxResponseTime": {
        "total": "1258",
        "ok": "1258",
        "ko": "93"
    },
    "meanResponseTime": {
        "total": "481",
        "ok": "623",
        "ko": "82"
    },
    "standardDeviation": {
        "total": "273",
        "ok": "155",
        "ko": "6"
    },
    "percentiles1": {
        "total": "495",
        "ok": "537",
        "ko": "82"
    },
    "percentiles2": {
        "total": "744",
        "ok": "756",
        "ko": "86"
    },
    "percentiles3": {
        "total": "811",
        "ok": "832",
        "ko": "92"
    },
    "percentiles4": {
        "total": "958",
        "ok": "973",
        "ko": "93"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 106,
        "percentage": 68
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 8,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 1,
        "percentage": 1
    },
    "group4": {
        "name": "failed",
        "count": 41,
        "percentage": 26
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "2.889",
        "ok": "2.13",
        "ko": "0.759"
    }
}
    },"req_delete--api-art-533c7": {
        type: "REQUEST",
        name: "DELETE /api/articles/{articleId}",
path: "DELETE /api/articles/{articleId}",
pathFormatted: "req_delete--api-art-533c7",
stats: {
    "name": "DELETE /api/articles/{articleId}",
    "numberOfRequests": {
        "total": "115",
        "ok": "27",
        "ko": "88"
    },
    "minResponseTime": {
        "total": "68",
        "ok": "434",
        "ko": "68"
    },
    "maxResponseTime": {
        "total": "885",
        "ok": "885",
        "ko": "126"
    },
    "meanResponseTime": {
        "total": "204",
        "ok": "596",
        "ko": "84"
    },
    "standardDeviation": {
        "total": "227",
        "ok": "133",
        "ko": "10"
    },
    "percentiles1": {
        "total": "84",
        "ok": "520",
        "ko": "82"
    },
    "percentiles2": {
        "total": "113",
        "ok": "722",
        "ko": "88"
    },
    "percentiles3": {
        "total": "730",
        "ok": "784",
        "ko": "100"
    },
    "percentiles4": {
        "total": "791",
        "ok": "862",
        "ko": "119"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 26,
        "percentage": 23
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 88,
        "percentage": 77
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "2.13",
        "ok": "0.5",
        "ko": "1.63"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
