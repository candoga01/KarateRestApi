@practice
Feature: Some practice test

  Background: Define BaseUrl
    Given url 'https://conduit.productionready.io/api/'


  @test
    #https://conduit.productionready.io/api/tags
  Scenario: Get all tags
    Given path 'tags'
    When method Get
    Then status 200
    And match response.tags contains ['HuManIty','Gandhi']
    And match response.tags !contains 'Gandhi2'
    And match response.tags == "#array"
    And match each response.tags == "#string"
    And match response.tags == "#array"
    And match response.tags == "#[10]"


    @testing
  Scenario: Get 10 articles from page
      #https://conduit.productionready.io/api/articles?limit=10&offset=0
    Given path 'articles'
    And params { limit:10, offset:0 }
    When method Get
    Then status 200
    And match response.articles == '#[10]'
    And match response.articles == '#array'
    And match response..favoritesCount == '#present'
    And match response..favorited == '#boolean'
      * def a = '.123'
      * def b = '.123'
      Then match a == b

  @fuzzy
  Scenario: Fuzzy matching

    * def foo = { }
    * match foo == { a: '##null' }
    * match foo == { a: '##notnull' }
    * match foo == { a: '#notpresent' }
    * match foo == { a: '#ignore' }

    * def foo = { a: null }
    * match foo == { a: '#null' }
    * match foo == { a: '##null' }
    * match foo == { a: '#present' }
    * match foo == { a: '#ignore' }

    * def foo = { a: 1 }
    * match foo == { a: '#notnull' }
    * match foo == { a: '##notnull' }
    * match foo == { a: '#present' }
    * match foo == { a: '#ignore' }

  Scenario: Post a new article
      #https://conduit.productionready.io/api/articles/
    Given path 'articles'
    When method Post
    Then status 200

