package gatling;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class CompareReports {

    public static void main(String[] args) throws IOException {
        String prev_report = args[0];
        String prev_build_number = args[1];
        String userDir = System.getProperty("user.dir");
        Workbook wb1 = WorkbookFactory.create(new File(userDir + "/src/test/resources/compareReports/prev_build/" + prev_report + ""));
        Workbook wb2 = WorkbookFactory.create(new File(userDir + "/target/gatling/build.xlsx"));
        CompareReports compareReports = new CompareReports();
        compareReports.verifyDataInExcelBookAllSheets(wb1, wb2, prev_build_number);
    }


    public void verifyDataInExcelBookAllSheets(Workbook workbook1, Workbook workbook2, String prev_build_number) throws IOException {
        String userDir = System.getProperty("user.dir");
        int sheetCounts = workbook1.getNumberOfSheets();
        for (int i = 0; i < sheetCounts; i++) {
            Sheet s1 = workbook1.getSheetAt(i);
            Sheet s2 = workbook2.getSheetAt(i);
            System.out.println("*********** Sheet Name : " + s1.getSheetName() + "*************");
            // Iterating through each row
            int rowCounts = s1.getPhysicalNumberOfRows();
            for (int j = 0; j < rowCounts; j++) {
                // Iterating through each cell
                int cellCounts = s1.getRow(j).getPhysicalNumberOfCells();
                int newColumn = cellCounts;
                for (int k = 3; k < cellCounts; k++) {
                    // Getting individual cell
                    Cell c1 = s1.getRow(j).getCell(k);
                    Cell c2 = s2.getRow(j).getCell(k);
                    Cell cell = s2.getRow(j).createCell(++newColumn);
                    if (c1.getCellType() == CellType.STRING) {
                        String v1 = c1.getStringCellValue();
                        String v2 = c2.getStringCellValue();
                        if (v1.equals(v2)) cell.setCellValue(v2);
                    }
                    if (c1.getCellType() == CellType.NUMERIC) {
                        double v1 = c1.getNumericCellValue();
                        double v2 = c2.getNumericCellValue();
                        double v3 = v1 - v2;
                        cell.setCellValue(v3);
                        if (v3 < 0) setColor(workbook2, cell, HSSFColor.HSSFColorPredefined.RED);
                        else setColor(workbook2, cell, HSSFColor.HSSFColorPredefined.GREEN);
                    }
                }
            }

            int totalRow = s2.getPhysicalNumberOfRows();
            int totalCell = s2.getRow(totalRow - 2).getPhysicalNumberOfCells();
            s2.createRow(totalRow);
            Row theCurrentRow = s2.getRow(totalRow);

            s2.addMergedRegion(new CellRangeAddress(totalRow, totalRow, totalCell - 2, totalCell));


            Cell cell = theCurrentRow.createCell(totalCell - 2);
            cell.setCellValue("BUILD #" + prev_build_number);
            setColor(workbook2, cell, HSSFColor.HSSFColorPredefined.WHITE);

            FileOutputStream outputStream = new FileOutputStream(new File(userDir + "/src/test/resources/compareReports/aggregate_report.xlsx"));
            workbook2.write(outputStream);
            outputStream.close();

        }
    }

    public static void setColor(Workbook wb, Cell cell, HSSFColor.HSSFColorPredefined colorPredefined) {
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setColor(colorPredefined.getIndex());
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        if (colorPredefined == HSSFColor.HSSFColorPredefined.WHITE) {
            style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        cell.setCellStyle(style);
    }
}
